import json
import os
import sys
from subprocess import run

if sys.version_info[0] < 3 or sys.version_info[1] < 6:
    sys.stderr.write("Python >=3.6 required")
    sys.exit(1)


def _compile_app(build_dir="build") -> None:
    if not os.path.exists(build_dir):
        os.mkdir(build_dir)

    os.chdir(build_dir)
    run(["cmake", ".."], check=True)
    run(["make"], check=True)
    os.chdir(os.pardir)


if __name__ == "__main__":

    paramfile = open("study-parameter.json",)
    study = json.load(paramfile)

    _compile_app("build")

    for case_id, resolution in enumerate(study["resolutions"]):

        case_name = f"case-{case_id:04d}"

        if (not os.path.exists(case_name)):
            os.mkdir(case_name)

        os.chdir(case_name)

        app_pathname = os.path.join(os.pardir, os.path.join("build", "myapp"))
        print (f"Running case {case_id}:")
        run([app_pathname, str(resolution)], check=True)
        os.chdir(os.pardir)
